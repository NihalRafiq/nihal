package ThisSuper;

class Myparent{
	String name="this is parent class";
}
public class MyClass extends Myparent {
	String name="this is instance variable";

	void somemethod() {
		String name= "this is local varible ...because i am inside method ";
		System.out.println(name);
		System.out.println(this.name);
		System.out.println(super.name);
	}

	public static void main(String[] args) {
		// TODO Auto-generated method stub
        MyClass mc = new MyClass();
        mc.somemethod();

	}

}
